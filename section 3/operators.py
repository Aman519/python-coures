a = 12
b = 3

print(a + b)    # 15
print(a - b)    # 9
print(a * b)    # 36
print(a / b)    # 4.0
print(a // b)   # 4 integer division, rounded down towards minus infinity
print(a % b)    # 0 modulo: the remainder after division, it will be 0 because 12 is divideable by 3, 4 times which contains no digits

print()

print(a + b / 3 - 4 * 12)
print(a + (b / 3) - (4 * 12))
print((((a + b) / 3) - 4) * 12)
print(((a + b) / 3 - 4) * 12)

c = 1 + b
d = c / 3
e = d - 4
print(e * 12)

print()

print(a / (b * a) /b)

# for i in range(1, a // b):
#     print(i)

# i = 1
# print(i)
# i = 2
# print(i)
# i = 3
# print(i)