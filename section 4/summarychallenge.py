options = ["1. Amusement park", "2. Gym", "3. Netflix", "4. Playing video games", "5. Going for a walk", "0. Exit"]

option_count = len(options) - 1

while True:
    for option in options:
        print(option)
    print("Select an activity for today by it's number")
    activity = int(input())
    if(activity == 0):
        print("You have exited the program")
        break
    elif(activity > 0 and activity <= option_count):
        print(options[activity - 1])
        break
    else:
        print("Please select a valid activity from the list")